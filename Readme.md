# Challanges in  the Low-Cost-Smart-Home

Hi! You are in the repository of my bachelor thesis. This can be reached at the following link. The video examples can be found in the folder video. The individual Dockerfiles are in the directory named code. There are also the Layout and the Fritzing files to find. 


# FHEM Docker

You have the choice between a local setup or a remotely controllable one. 

## Running the Containers

Make sure that you have properly installed docker and docker-compose on your machine. If you need help, you can read here: https://docs.docker.com/install/

Change to the directory code/docker-compose/smarthomedocker for the local method without the secure remote access. Just type following in code into your Terminal

`docker-compose build && docker-compose up`

Now the FHEM Webinterface is available under http://hostip:8083/fhem

## Setup remote access

If you want to control your home safely from anywhere, you can use the second method. Make sure your host is reachable via ports 80 and 443 over the internet. Before you start you have to tell Let's Encrypt the URL, and a valid Email Adress for your Domain. Change these settings in the docker-compose.yml file.

      environment:
      - EMAIL=me@example.com
      - URL=example.duckdns.org

Note that yml files are whitespace sensitive. Maybe you need a ddns service, such as https://duckdns.org


