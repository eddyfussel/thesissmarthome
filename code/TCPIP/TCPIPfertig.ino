#include <SPI.h>
#include <Ethernet.h>

// ### CONFIG ###

// Ethernet
byte mac[] = {0xAB, 0xCD, 0x00, 0x11, 0x22, 0x33};
IPAddress ip(192, 168, 105, 99);
IPAddress myDns(192,168,105, 1);
IPAddress gateway(192, 168, 105, 1);
IPAddress subnet(255, 255, 255, 0);

// Serial TTL
int serialBaud = 9600;      // DENON needs 9600, SANYO needs 19200 (initial)
int serialCfg = SERIAL_8N1; // DENON an SANYO need  SERIAL_8N1

// Server
EthernetServer server(20100);

// ### SETUP ###

void setup() {
  
  // Initialize the ethernet device
  Ethernet.begin(mac, ip, myDns, gateway, subnet);
  // Start listening for clients
  server.begin();
  // Open serial communications and wait for port to open:
  Serial.begin(serialBaud, serialCfg);
  // Prints IP Address for testing
  //Serial.println(Ethernet.localIP());
}

// ### LOOP ###

void loop() {
  // Wait for a new client
  EthernetClient client = server.available();
  String cmd ="";
  if (client) {
    while (client.connected()) {
      // TCP-->RS232
      while (client.available()) {
        char c = client.read();
        cmd+=c;
        if (c == '\r') {
          Serial.print(cmd);
          cmd = "";
          Serial.flush();
        }
      }
      // RS232-->TCP
      int msg = 0;
      while (Serial.available() > 0) {
        msg = Serial.read();
        client.write(msg);
      }
    }
  }
}
