# $Id$ 71_DENON_SERIAL.pm 2016-06-22 09:30:00 masterk $
##############################################################################
#
#	  71_DENON_SERIAL.pm
#	  An FHEM Perl module for controlling Denon AV-Receivers
#	  via network connection.
#
#     Currently supported are:  power (on|off)
#                               volumeStraight (-80 ... 18)
#                               volume (0 ... 98)
#                               mute (on|off)
#				input (select input source)
#				sound (select sound mode)
#				favorite (1|2|3)
#				preset (P1|P2|P3)
#
#     In addition, you can send any documented command from the "DENON AVR
#     protocol documentation" via "rawCommand <command>"; e.g. "rawCommand
#     PWON" does the exact same thing as "power on"
#
#	  Copyright by Boris Pruessmann
#
#         forked by xusader/michaelmueller
#		forked by quigley
#		forked by masterk
#   forked by eddyfussel
#
#		define myDenon DENON_SERIAL 192.168.x.x:20100 for Arduino TCPIP
#		or define for serial port:
#
#			- Favorites can be called now
#			- Fixed bug while setting volume to values less than 10
#			- Code cleanup at SetVolume function
#				forked by Amenophis86
#			- rawCommand mit Leerzeichen
#			- reconnect / disconnect
#			- SimpleWrite update
#
#	  This file is part of fhem.
#
#	  Fhem is free software: you can redistribute it and/or modify
#	  it under the terms of the GNU General Public License as published by
#	  the Free Software Foundation, either version 2 of the License, or
#	  (at your option) any later version.
#
#	  Fhem is distributed in the hope that it will be useful,
#	  but WITHOUT ANY WARRANTY; without even the implied warranty of
#	  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#	  GNU General Public License for more details.
#
#	  You should have received a copy of the GNU General Public License
#	  along with fhem.	If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
package main;

use strict;
use warnings;

use Time::HiRes qw(usleep gettimeofday);

###################################
my %commands =
(
	"power:on" => "PWON",
	"power:off" => "PWSTANDBY",
	"mute:on" => "MUON",
	"mute:off" => "MUOFF",
	"z2:on" => "Z2ON",
	"z2:off" => "Z2OFF",
	"mutez2:on" => "Z2MUON",
	"mutez2:off" => "Z2MUOFF"
);

my %powerStateTransition =
(
	"on"  => "off",
	"off" => "on"
);

my %inputs =
(
	    "TUNER" => "",
	    "DVD" => "",
	    "CD" => "",
	    "TV" => "",
	    "VCR-1" => "",
	    "VCR-2" => "",
	    "V.AUX" => "",
	    "CDR/TAPE" => ""
);

my %z2inputs =
(
	    "TUNER" => "",
	    "DVD" => "",
	    "CD" => "",
	    "TV" => "",
	    "VCR-1" => "",
	    "VCR-2" => "",
	    "V.AUX" => "",
	    "CDR/TAPE" => ""
);

my %sounds =
(
	    "MOVIE" => "",
	    "MUSIC" => "",
	    "GAME" => "",
	    "DIRECT" => "",
	    "STEREO" => "",
	    "STANDARD" => "",
	    "DOLBY_DIGITAL" => "",
	    "DTS_SURROUND" => "",
	    "MCH_STEREO" => "",
	    "ROCK_ARENA" => "",
	    "JAZZ_CLUB" => "",
	    "MONO_MOVIE" => "",
	    "MATRIX" => "",
	    "VIDEO_GAME" => "",
	    "VIRTUAL" => ""
);

###################################
sub
DENON_SERIAL_Initialize($)
{
	my ($hash) = @_;

	Log 5, "DENON_SERIAL_Initialize: Entering";

	require "$attr{global}{modpath}/FHEM/DevIo.pm";

# Provider
	$hash->{ReadFn}	 = "DENON_SERIAL_Read";
	$hash->{WriteFn} = "DENON_SERIAL_Write";

# Device
	$hash->{DefFn}		= "DENON_SERIAL_Define";
	$hash->{UndefFn}	= "DENON_SERIAL_Undefine";
	$hash->{GetFn}		= "DENON_SERIAL_Get";
	$hash->{SetFn}		= "DENON_SERIAL_Set";
	$hash->{AttrFn}     	= "DENON_SERIAL_Attr";
	$hash->{ShutdownFn} 	= "DENON_SERIAL_Shutdown";

	$hash->{AttrList}  = "do_not_notify:0,1 loglevel:0,1,2,3,4,5 do_not_send_commands:0,1 keepalive ".$readingFnAttributes;
}

#####################################
sub
DENON_SERIAL_DoInit($)
{
	my ($hash) = @_;
	my $name = $hash->{NAME};

	my $ll5 = GetLogLevel($name, 5);
	Log $ll5, "DENON_SERIAL_DoInit: Called for $name";

	DENON_SERIAL_Command_StatusRequest($hash);

	$hash->{STATE} = "Initialized";

	return undef;
}

###################################
sub
DENON_SERIAL_Read($)
{
	my ($hash) = @_;
	my $name = $hash->{NAME};

	my $ll5 = GetLogLevel($name, 5);
	Log $ll5, "DENON_SERIAL_Read: Called for $name";

	my $buf = DevIo_SimpleRead($hash);
	return "" if (!defined($buf));

	my $culdata = $hash->{PARTIAL};
	Log $ll5, "DENON_SERIAL_Read: $culdata/$buf";
	$culdata .= $buf;

	readingsBeginUpdate($hash);
	while ($culdata =~ m/\r/)
	{
		my $rmsg;
		($rmsg, $culdata) = split("\r", $culdata, 2);
		$rmsg =~ s/\r//;

		DENON_SERIAL_Parse($hash, $rmsg) if($rmsg);
	}
	readingsEndUpdate($hash, 1);

	$hash->{PARTIAL} = $culdata;
}

#####################################
sub
DENON_SERIAL_Write($$$)
{
	my ($hash, $fn, $msg) = @_;

	Log 5, "DENON_SERIAL_Write: Called";
}

###################################
###################################
sub
DENON_SERIAL_SimpleWrite(@)
{
	my ($hash, $msg) = @_;
	my $name = $hash->{NAME};

	my $ll5 = GetLogLevel($name, 5);
	Log $ll5, "DENON_SERIAL_SimpleWrite: $msg";

	my $doNotSendCommands = AttrVal($name, "do_not_send_commands", "0");
	if ($doNotSendCommands ne "1")
	{
		#syswrite($hash->{TCPDev}, $msg."\r") if ($hash->{TCPDev});
		#$hash->{USBDev}->write($msg."\r")    if($hash->{USBDev});
	   DevIo_SimpleWrite($hash, $msg."\r", 0);

		# Let's wait 100ms - not sure if still needed
		usleep(100 * 1000);

		# Some linux installations are broken with 0.001, T01 returns no answer
		select(undef, undef, undef, 0.01);
	}
}

###################################
sub
DENON_SERIAL_Parse(@)
{
	my ($hash, $msg) = @_;
	my $name = $hash->{NAME};

	my $ll5 = GetLogLevel($name, 5);
	Log $ll5, "DENON_SERIAL_Parse: Parsing <$msg>";

	if ($msg =~ /PW(.+)/)
	{
		my $power = lc($1);
		if ($power eq "standby")
		{
			$power = "off";
		}

		readingsBulkUpdate($hash, "power", $power);
		$hash->{STATE} = $power;
	}
	elsif ($msg =~ /MU(.+)/)
	{
		readingsBulkUpdate($hash, "mute", lc($1));
	}
	elsif ($msg =~ /MVMAX (.+)/)
	{
		Log $ll5, "DENON_SERIAL_Parse: Ignoring maximum volume of <$1>";
	}
	elsif ($msg =~ /MV(.+)/)
	{
		my $volume = $1;
		if (length($volume) == 2)
		{
			$volume = $volume."0";
		}
		readingsBulkUpdate($hash, "volumeStraight", $volume / 10 - 80);
		readingsBulkUpdate($hash, "volume", $volume / 10);
	}
	elsif ($msg =~ /CVFL(.+)/)
	{
		my $volumeFL = $1;
		if (length($volumeFL) == 4)
		{
			$volumeFL = $volumeFL / 10;
		}
		readingsBulkUpdate($hash, "cvFL", $volumeFL - 50);
	}
	elsif ($msg =~ /CVFR(.+)/)
	{
		my $volumeFR = $1;
		if (length($volumeFR) == 4)
		{
			$volumeFR = $volumeFR / 10;
		}
		readingsBulkUpdate($hash, "cvFR", $volumeFR - 50);
	}
	elsif ($msg =~ /CVC(.+)/)
	{
		my $volumeC = $1;
		if (length($volumeC) == 4)
		{
			$volumeC = $volumeC / 10;
		}
		readingsBulkUpdate($hash, "cvC", $volumeC - 50);
	}
	elsif ($msg =~ /CVSW(.+)/)
	{
		my $volumeSW = $1;
		if (length($volumeSW) == 4)
		{
			$volumeSW = $volumeSW / 10;
		}
		readingsBulkUpdate($hash, "cvSW", $volumeSW - 50);
	}
	elsif ($msg =~ /CVSL(.+)/)
	{
		my $volumeSL = $1;
		if (length($volumeSL) == 4 )
		{
			$volumeSL = $volumeSL / 10;
		}
		readingsBulkUpdate($hash, "cvSL", $volumeSL - 50);
	}
	elsif ($msg =~ /CVSR(.+)/)
	{
		my $volumeSR = $1;
		if (length($volumeSR) == 4)
		{
			$volumeSR = $volumeSR / 10;
		}
		readingsBulkUpdate($hash, "cvSR", $volumeSR -50);
	}
	elsif ($msg =~/SI(.+)/)
	{
		readingsBulkUpdate($hash, "input", $1);
	}
	elsif ($msg =~/Z2(.+)/ && $msg !=~/Z2MU(.+)/)
	{
		readingsBulkUpdate($hash, "z2input", $1);
	}
	elsif ($msg =~/MS(.+)/)
	{
		readingsBulkUpdate($hash, "sound", $1);
	}
	else
	{
		Log $ll5, "DENON_SERIAL_Parse: Unknown message <$msg>";
	}
}

###################################
sub
DENON_SERIAL_Define($$)
{
	my ($hash, $def) = @_;

	Log 5, "DENON_SERIAL_Define($def) called.";

	my @a = split("[ \t][ \t]*", $def);

	if (@a != 3)
	{
		my $msg = "wrong syntax: define <name> DENON_SERIAL <ip-or-hostname>";
		Log 2, $msg;

		return $msg;
	}

	DevIo_CloseDev($hash);

	my $name = $a[0];
	my $host = $a[2];
	$hash->{DeviceName} = $host.":20100";
	my $ret = DevIo_OpenDev($hash, 0, "DENON_SERIAL_DoInit");

	InternalTimer(gettimeofday() + 5, "DENON_SERIAL_UpdateConfig", $hash, 0);

	unless (exists($attr{$name}{webCmd})){
		$attr{$name}{webCmd} = 'volumeStraight:mute:input:z2input:sound:favorite:preset';
	}
	unless (exists($attr{$name}{stateFormat})){
		$attr{$name}{stateFormat} = 'power';
	}

	return $ret;
}

#############################
sub
DENON_SERIAL_Undefine($$)
{
	my($hash, $name) = @_;

	Log 5, "DENON_SERIAL_Undefine: Called for $name";

	RemoveInternalTimer($hash);
	DevIo_CloseDev($hash);

	return undef;
}

#############################
sub
DENON_SERIAL_Get($@)
{
	my ($hash, @a) = @_;
	my $what;

	return "argument is missing" if (int(@a) != 2);
	$what = $a[1];

	if ($what =~ /^(power|volumeStraight|volume|volumeDown|volumeUp|mute|input|z2input|sound|cvFL|cvFR|cvC|cvSW|cvSL|cvSR)$/)
	{
		if(defined($hash->{READINGS}{$what}))
		{

			return $hash->{READINGS}{$what}{VAL};
		}
		else
		{
			return "no such reading: $what";
		}
	}
	else
	{
		return "Unknown argument $what, choose one of power volumeStraight volume volumeDown volumeUp mute input sound";
	}
}

###################################
sub
DENON_SERIAL_Set($@)
{
	my ($hash, @a) = @_;

	my $what = $a[1];

	my $usage = "Unknown argument $what, choose one of favorite:1,2,3 preset:P1,P2,P3 on off toggle volumeDown volumeUp volumeStraight:slider,-80,1,18 volume:slider,0,1,98  cvFL:slider,-12,1,12 cvFR:slider,-12,1,12 cvSL:slider,-12,1,12 cvSR:slider,-12,1,12 cvC:slider,-12,1,12 cvSW:slider,-12,1,12 mute:on,off reconnect disconnect " .
		    "input:" . join(",", sort keys %inputs) . " " .
			"z2input:" . join(",", sort keys %z2inputs) . " " .
		    "sound:" . join(",", sort keys %sounds) . " " .
		    "rawCommand statusRequest";

	if ($what =~ /^(on|off)$/)
	{
		return DENON_SERIAL_Command_SetPower($hash, $what);
	}
	elsif ($what eq "favorite")
	{
		my $favorite = $a[2];
		return DENON_SERIAL_Command_SetFavorite($hash, $favorite);
	}
	elsif ($what eq "preset")
	{
		my $preset = $a[2];
		return DENON_SERIAL_Command_SetPreset($hash, $preset);
	}
	elsif ($what eq "toggle")
	{
		my $newPowerState = $powerStateTransition{$hash->{STATE}};
		return $newPowerState if (!defined($newPowerState));

		return DENON_SERIAL_Command_SetPower($hash, $newPowerState);
	}
	elsif ($what eq "mute")
	{
		my $mute = $a[2];
		return DENON_SERIAL_Command_SetMute($hash, $mute);
	}
	elsif ($what eq "input")
	{
		my $input = $a[2];
		return DENON_SERIAL_Command_SetInput($hash, $input);
	}
	elsif ($what eq "z2input")
	{
		my $z2input = $a[2];
		return DENON_SERIAL_Command_SetInputZ2($hash, $z2input);
	}
	elsif ($what eq "sound")
	{
		my $sound = $a[2];

		if (	 $sound eq "DOLBY_DIGITAL") {
		    $sound = "DOLBY DIGITAL";

		} elsif ($sound eq "DTS_SURROUND") {
		    $sound = "DTS SURROUND";
		}
		elsif ($sound eq "MCH_STEREO") {
		    $sound = "MCH STEREO";
		}
		elsif ($sound eq "ROCK_ARENA") {
		    $sound = "ROCK ARENA";
		}
		elsif ($sound eq "JAZZ_CLUB") {
		    $sound = "JAZZ CLUB";
		}
		elsif ($sound eq "MONO_MOVIE") {
		    $sound = "MONO MOVIE";
		}
		elsif ($sound eq "VIDEO_GAME") {
		    $sound = "VIDEO GAME";
		}

		return DENON_SERIAL_Command_SetSound($hash, $sound);
	}
	elsif ($what eq "volumeStraight")
	{
		my $volume = $a[2];
		return DENON_SERIAL_Command_SetVolume($hash, $volume + 80);
	}
	elsif ($what eq "volume")
	{
		my $volume = $a[2];
		return DENON_SERIAL_Command_SetVolume($hash, $volume);
	}
	elsif ($what eq "cvFL")
	{
		my $volume = $a[2];
		return DENON_SERIAL_Command_SetChannelVolume($hash, $volume, "FL");
	}
	elsif ($what eq "cvFR")
	{
		my $volume = $a[2];
		return DENON_SERIAL_Command_SetChannelVolume($hash, $volume, "FR");
	}
	elsif ($what eq "cvSL")
	{
		my $volume = $a[2];
		return DENON_SERIAL_Command_SetChannelVolume($hash, $volume, "SL");
	}
	elsif ($what eq "cvSR")
	{
		my $volume = $a[2];
		return DENON_SERIAL_Command_SetChannelVolume($hash, $volume, "SR");
	}
	elsif ($what eq "cvC")
	{
		my $volume = $a[2];
		return DENON_SERIAL_Command_SetChannelVolume($hash, $volume, "C");
	}
	elsif ($what eq "cvSW")
	{
		my $volume = $a[2];
		return DENON_SERIAL_Command_SetChannelVolume($hash, $volume, "SW");
	}
	elsif ($what eq "volumeDown")
	{
		my $cmd = "MVDOWN";
		DENON_SERIAL_SimpleWrite($hash, $cmd);
	}
	elsif ($what eq "volumeUp")
	{
		my $cmd = "MVUP";
		DENON_SERIAL_SimpleWrite($hash, $cmd);
	}
	elsif ($what eq "rawCommand")
	{
		my $cmd = $a[2];
		$cmd = $a[2]." ".$a[3] if defined $a[3];
		DENON_SERIAL_SimpleWrite($hash, $cmd);
	}
	elsif ($what eq "statusRequest")
	{
	# Force update of status
	return DENON_SERIAL_Command_StatusRequest($hash);
	}
	elsif ($what eq "reconnect")
	{
		my $status = $hash->{READINGS}{"state"}{VAL};
		if($status ne "opened")
		{
			DevIo_OpenDev($hash, 0, "DENON_SERIAL_DoInit");
		}
		else
		{
			return "Device must be connected to disconnect!";
		}
	}
	elsif ($what eq "disconnect")
	{
			my $name = $hash->{NAME};
			DevIo_CloseDev($hash);
			$hash->{STATE} = "disconnected";
			setReadingsVal($hash, "state", "disconnected", TimeNow());
			Log 1, "$name: closed!";
	}
	else
	{
	return $usage;
	}
    return undef;
}

###################################
sub
DENON_SERIAL_Attr($@)
{
	my @a = @_;

	my $what = $a[2];
	if ($what eq "keepalive")
	{
		my $name = $a[1];
	    	my $hash = $defs{$name};

		my $keepalive = $a[3];

		my $ll5 = GetLogLevel($name, 5);
		Log $ll5, "DENON_SERIAL_Attr: Changing keepalive to <$keepalive> seconds";

		RemoveInternalTimer($hash);
		InternalTimer(gettimeofday() + $keepalive, "DENON_SERIAL_KeepAlive", $hash, 0);
	}

	return undef;
}

#####################################
sub
DENON_SERIAL_Shutdown($)
{
	my ($hash) = @_;

	Log 5, "DENON_SERIAL_Shutdown: Called";
}

#####################################
sub
DENON_SERIAL_UpdateConfig($)
{
	# this routine is called 5 sec after the last define of a restart
	# this gives FHEM sufficient time to fill in attributes
	# it will also be called after each manual definition
	# Purpose is to parse attributes and read config
	my ($hash) = @_;
	my $name = $hash->{NAME};

	my $webCmd	= AttrVal($name, "webCmd", "");
	if (!$webCmd)
	{
		$attr{$name}{webCmd} = "volumeStraight:mute:input:z2input:sound";
	}

	my $keepalive = AttrVal($name, "keepalive", 5 * 60);

	RemoveInternalTimer($hash);
	InternalTimer(gettimeofday() + $keepalive, "DENON_SERIAL_KeepAlive", $hash, 0);
}

#####################################
sub
DENON_SERIAL_KeepAlive($)
{
	my ($hash) = @_;
	my $name = $hash->{NAME};

	my $ll5 = GetLogLevel($name, 5);
	Log $ll5, "DENON_SERIAL_KeepAlive: Called for $name";

	DENON_SERIAL_SimpleWrite($hash, "PW?");

	my $keepalive = AttrVal($name, "keepalive", 5 * 60);

	RemoveInternalTimer($hash);
	InternalTimer(gettimeofday() + $keepalive, "DENON_SERIAL_KeepAlive", $hash, 0);
}

#####################################
sub
DENON_SERIAL_Command_SetPower($$)
{
	my ($hash, $power) = @_;
	my $name = $hash->{NAME};

	my $ll5 = GetLogLevel($name, 5);
	Log $ll5, "DENON_SERIAL_Command_SetPower: Called for $name";

	my $command = $commands{"power:".lc($power)};
	DENON_SERIAL_SimpleWrite($hash, $command);

	readingsBeginUpdate($hash);
	readingsBulkUpdate($hash, "power", $power);
	readingsEndUpdate($hash, 1);

	return undef;
}

#####################################
sub
DENON_SERIAL_Command_SetMute($$)
{
	my ($hash, $mute) = @_;
	my $name = $hash->{NAME};

	my $ll5 = GetLogLevel($name, 5);
	Log $ll5, "DENON_SERIAL_Command_SetMute: Called for $name";

	return "mute can only used when device is powered on" if ($hash->{STATE} eq "off");

	my $command = $commands{"mute:".lc($mute)};
	DENON_SERIAL_SimpleWrite($hash, $command);

	return undef;
}

#####################################
sub
DENON_SERIAL_Command_SetInput($$)
{
	my ($hash, $input) = @_;
	my $name = $hash->{NAME};

	my $ll5 = GetLogLevel($name, 5);
	Log $ll5, "DENON_SERIAL_Command_SetInput: Called for $name";

	DENON_SERIAL_SimpleWrite($hash, "SI".$input);
	readingsBeginUpdate($hash);
	readingsBulkUpdate($hash, "input", $input);
	readingsEndUpdate($hash, 1);

	return undef;
}

#####################################
sub
DENON_SERIAL_Command_SetInputZ2($$)
{
	my ($hash, $z2input) = @_;
	my $name = $hash->{NAME};

	my $ll5 = GetLogLevel($name, 5);
	Log $ll5, "DENON_SERIAL_Command_SetInput: Called for $name";

	DENON_SERIAL_SimpleWrite($hash, "Z2".$z2input);
	readingsBeginUpdate($hash);
	readingsBulkUpdate($hash, "z2input", $z2input);
	readingsEndUpdate($hash, 1);

	return undef;
}

#####################################
sub
DENON_SERIAL_Command_SetSound($$)
{
	my ($hash, $sound) = @_;
	my $name = $hash->{NAME};

	my $ll5 = GetLogLevel($name, 5);
	Log $ll5, "DENON_SERIAL_Command_SetSound: Called for $name";

	DENON_SERIAL_SimpleWrite($hash, "MS".$sound);
	readingsBeginUpdate($hash);
	readingsBulkUpdate($hash, "sound", $sound);
	readingsEndUpdate($hash, 1);
	return undef;
}

#####################################
sub
DENON_SERIAL_Command_SetFavorite($$)
{
	my ($hash, $favorite) = @_;
	my $name = $hash->{NAME};

	my $ll5 = GetLogLevel($name, 5);
	Log $ll5, "DENON_SERIAL_Command_SetFavorite: Called for $name";

	DENON_SERIAL_SimpleWrite($hash, "ZMFAVORITE".$favorite);

	return undef;
}
#####################################
sub
DENON_SERIAL_Command_SetPreset($$)
{
	my ($hash, $preset) = @_;
	my $name = $hash->{NAME};

	my $ll5 = GetLogLevel($name, 5);
	Log $ll5, "DENON_SERIAL_Command_SetPreset: Called for $name";

	DENON_SERIAL_SimpleWrite($hash, "NS".$preset);

	return undef;
}

#####################################
sub
DENON_SERIAL_Command_SetVolume($$)
{
	my ($hash, $volume) = @_;
	my $name = $hash->{NAME};

	my $ll5 = GetLogLevel($name, 5);
	Log $ll5, "DENON_SERIAL_Command_SetVolume: Called for $name";

	if($hash->{STATE} eq "off")
	{
		return "volume can only used when device is powered on";
	}
	else
	{
		if ($volume % 1 == 0)
		{
			$volume = sprintf ('%03d', ($volume * 10));
		}
		else
		{
			$volume = sprintf ('%02d', $volume);
		}
		DENON_SERIAL_SimpleWrite($hash, "MV".$volume);
	}

	return undef;
}

#####################################
sub
DENON_SERIAL_Command_SetChannelVolume($$$)
{
	my ($hash, $volume, $channel) = @_;
	my $name = $hash->{NAME};

	my $ll5 = GetLogLevel($name, 5);
	Log $ll5, "DENON_SERIAL_Command_SetVolume: Called for $name";

	if($hash->{STATE} eq "off")
	{
		return "volume can only used when device is powered on";
	}
	else
	{
		if ($volume % 1 == 0)
		{
			$volume =  $volume + 50;
		}
		else
		{
			$volume =  $volume * 10 + 50;
		}
		DENON_SERIAL_SimpleWrite($hash, "CV".$channel." ".$volume);
	}

	return undef;
}

#####################################
sub
DENON_SERIAL_Command_StatusRequest($)
{
 	my ($hash) = @_;

	my $name = $hash->{NAME};

	my $ll5 = GetLogLevel($name, 5);
	Log $ll5, "DENON_SERIAL_Command_StatusRequest: Called for $name";

	DENON_SERIAL_SimpleWrite($hash, "PW?");
	DENON_SERIAL_SimpleWrite($hash, "MU?");
	DENON_SERIAL_SimpleWrite($hash, "MV?");
	DENON_SERIAL_SimpleWrite($hash, "SI?");
	DENON_SERIAL_SimpleWrite($hash, "MS?");
	DENON_SERIAL_SimpleWrite($hash, "NSP");
	DENON_SERIAL_SimpleWrite($hash, "ZM?");
	DENON_SERIAL_SimpleWrite($hash, "Z2?");
	DENON_SERIAL_SimpleWrite($hash, "CV?");

	return undef;
}

1;
