# $Id$ 71_SANYO_Z2000.pm 2016-08-10 09:30:00 masterk $
################################################################################
#
#	  71_SANYO_Z2000.pm
#	  An FHEM Perl module for controlling Sanyo Beamer Z2000 via
#	  socat->Ethernet->RS232
#
#     Currently supported are:
#
#     In addition, you can send any documented command from the "SANYO BEAMER
#     protocol documentation" via "rawCommand <command>"
#
#	  Copyright by eddytracker
#
#		define for serial port:
#		define mySanyo SANYO_Z2000 /dev/ttyUSB0@9600
#
#	  This file is part of fhem.
#
#	  Fhem is free software: you can redistribute it and/or modify
#	  it under the terms of the GNU General Public License as published by
#	  the Free Software Foundation, either version 2 of the License, or
#	  (at your option) any later version.
#
#	  Fhem is distributed in the hope that it will be useful,
#	  but WITHOUT ANY WARRANTY; without even the implied warranty of
#	  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#	  GNU General Public License for more details.
#
#	  You should have received a copy of the GNU General Public License
#	  along with fhem.	If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
package main;

use strict;
use warnings;
use Switch;

use Time::HiRes qw(usleep gettimeofday);

###################################
my %commands =
(
	"power:on" => "C00",
	"power:off" => "C01",
	"videomute:on" => "C0D",
	"videomute:off" => "C0E",
	"freeze:on" => "C43",
	"freeze:off" => "C44"
);


my %powerStateTransition =
(
	"on"  => "off",
	"off" => "on"
);

my %inputs =
(
	    "HDMI1" => "C53",
	    "HDMI2" => "C54",
	    "RGB" => "C50",
	    "Scart" => "C51"
);

my %images =
(
	"Living" => "C11",
	"PCinema" => "C13",
	"BCinema" => "C19",
	"CCimema" => "C12",
	"Dynamic" => "C1A",
	"Natural" => "C1B",
	"Vivid" => "C18"
);

my %zooms =
(
	"Standart" => "C0F",
	"Vollbild" => "C10",
	"Zoom" => "C2C"
);

###################################
sub
SANYO_Z2000_Initialize($)
{
	my ($hash) = @_;

	Log 5, "SANYO_Z2000_Initialize: Entering";

	require "$attr{global}{modpath}/FHEM/DevIo.pm";

# Provider
	$hash->{ReadFn}	 = "SANYO_Z2000_Read";
	$hash->{WriteFn} = "SANYO_Z2000_Write";

# Device
	$hash->{DefFn}		= "SANYO_Z2000_Define";
	$hash->{UndefFn}	= "SANYO_Z2000_Undefine";
	$hash->{GetFn}		= "SANYO_Z2000_Get";
	$hash->{SetFn}		= "SANYO_Z2000_Set";
	$hash->{AttrFn}     	= "SANYO_Z2000_Attr";
	$hash->{ShutdownFn} 	= "SANYO_Z2000_Shutdown";

	$hash->{AttrList}  = "do_not_notify:0,1 loglevel:0,1,2,3,4,5 do_not_send_commands:0,1 keepalive ".$readingFnAttributes;
}

#####################################
sub
SANYO_Z2000_DoInit($)
{
	my ($hash) = @_;
	my $name = $hash->{NAME};

	my $ll5 = GetLogLevel($name, 5);
	Log $ll5, "SANYO_Z2000_DoInit: Called for $name";

	SANYO_Z2000_Command_StatusRequest($hash);

	$hash->{STATE} = "Initialized";

	return undef;
}

###################################
sub
SANYO_Z2000_Read($)
{
	my ($hash) = @_;
	my $name = $hash->{NAME};

	my $ll5 = GetLogLevel($name, 5);
	Log $ll5, "SANYO_Z2000_Read: Called for $name";

	my $buf = DevIo_SimpleRead($hash);
	return "" if (!defined($buf));

	my $culdata = $hash->{PARTIAL};
	Log 3, "SANYO_Z2000_Read: $culdata/$buf";
	$culdata .= $buf;

	readingsBeginUpdate($hash);
	while ($culdata =~ m/\r/)
	{
		my $rmsg;
		($rmsg, $culdata) = split("\r", $culdata, 2);
		$rmsg =~ s/\r//;

		SANYO_Z2000_Parse($hash, $rmsg) if($rmsg);
	}
	readingsEndUpdate($hash, 1);

	$hash->{PARTIAL} = $culdata;
}

#####################################
sub
SANYO_Z2000_Write($$$)
{
	my ($hash, $fn, $msg) = @_;

	Log 5, "SANYO_Z2000_Write: Called";
}

###################################
###################################
sub
SANYO_Z2000_SimpleWrite(@)
{
	my ($hash, $msg) = @_;
	my $name = $hash->{NAME};

	my $ll5 = GetLogLevel($name, 5);
	Log 3, "SANYO_Z2000_SimpleWrite: $msg";

	my $doNotSendCommands = AttrVal($name, "do_not_send_commands", "0");
	if ($doNotSendCommands ne "1")
	{
	   DevIo_SimpleWrite($hash, $msg."\r", 0);

		# Let's wait 100ms - not sure if still needed
		usleep(100 * 1000);

		# Some linux installations are broken with 0.001, T01 returns no answer
		select(undef, undef, undef, 0.01);
	}
}

###################################
sub
SANYO_Z2000_Parse(@)
{
	my ($hash, $msg) = @_;
	my $name = $hash->{NAME};

	my $ll5 = GetLogLevel($name, 5);
	Log 3, "SANYO_Z2000_Parse: Parsing <$msg>";

  switch ($msg) {
		case "00"
		{
			my $power = "on";
			readingsBulkUpdate($hash, "power", $power);
			$hash->{STATE} = $power;
			my $status = "Power ON";
			readingsBulkUpdate($hash, "status", $status);
			$hash->{STATE} = $status;
		}
		case "80"
		{
		  my $power = "off";
		  readingsBulkUpdate($hash, "power", $power);
		  $hash->{STATE} = $power;
		  my $status = "Standby";
		  readingsBulkUpdate($hash, "status", $status);
		  $hash->{STATE} = $status;
		}
		case "40"
		{
		  my $status = "Processing Countdown";
		  readingsBulkUpdate($hash, "status", $status);
		  $hash->{STATE} = $status;
		}
		case "20"
		{
		  my $status = "Processing Coolingdown";
		  readingsBulkUpdate($hash, "status", $status);
		  $hash->{STATE} = $status;
		}
		case "10"
		{
		  my $status = "Power failure";
		  readingsBulkUpdate($hash, "status", $status);
		  $hash->{STATE} = $status;
		}
		case "28"
		{
		  my $status = "Processing Countdown due abnormal temperature";
		  readingsBulkUpdate($hash, "status", $status);
		  $hash->{STATE} = $status;
		}
		case "88"
		{
		  my $status = "Standby after abnormal temperature";
		  readingsBulkUpdate($hash, "status", $status);
		  $hash->{STATE} = $status;
		}
		case  "24"
		{
		  my $status = "Processing Powersafe";
		  readingsBulkUpdate($hash, "status", $status);
		  $hash->{STATE} = $status;
		}
		case "04"
		{
		  my $status = "Powersafe";
		  readingsBulkUpdate($hash, "status", $status);
		  $hash->{STATE} = $status;
		}
		case "21"
		{
		  my $status = "Processing cooling down after lampfailure";
		  readingsBulkUpdate($hash, "status", $status);
		  $hash->{STATE} = $status;
		}
		case "81"
		{
		  my $status = "Standby after lamp failure";
		  readingsBulkUpdate($hash, "status", $status);
		  $hash->{STATE} = $status;
		}
		case "4"
		{
		  my $input = "HDMI1";
		  readingsBulkUpdate($hash, "input", $input);
		  $hash->{STATE} = $input;
		}
		case "5"
		{
		  my $input = "HDMI2";
		  readingsBulkUpdate($hash, "input", $input);
		  $hash->{STATE} = $input;
		}
		case "6"
		{
		  my $input = "RGB";
		  readingsBulkUpdate($hash, "input", $input);
		  $hash->{STATE} = $input;
		}
		case "7"
		{
		  my $input = "Scart";
		  readingsBulkUpdate($hash, "input", $input);
		  $hash->{STATE} = $input;
		}
		case (/[0-9]{5}/)
		{
		  my $lamp = "200";
		  #Log 3, "Huiiiii";
		  readingsBulkUpdate($hash, "lamp", $msg);
		  $hash->{STATE} = $lamp;
		}
		else
		{
		  Log 3, "SANYO_Z2000_Parse: Unknown message <$msg>";
		}
	}
}

###################################
sub
SANYO_Z2000_Define($$)
{
	my ($hash, $def) = @_;

	Log 5, "SANYO_Z2000_Define($def) called.";

	my @a = split("[ \t][ \t]*", $def);

	if (@a != 3)
	{
		my $msg = "wrong syntax: define <name> SANYO_Z2000 <ip-or-hostname>";
		Log 2, $msg;

		return $msg;
	}

	DevIo_CloseDev($hash);

	my $name = $a[0];
	my $host = $a[2];
	$hash->{DeviceName} = $host.":20100";
	my $ret = DevIo_OpenDev($hash, 0, "SANYO_Z2000_DoInit");

	InternalTimer(gettimeofday() + 5, "SANYO_Z2000_UpdateConfig", $hash, 0);

	unless (exists($attr{$name}{webCmd})){
		$attr{$name}{webCmd} = 'input';
	}
	unless (exists($attr{$name}{stateFormat})){
		$attr{$name}{stateFormat} = 'power';
	}

	return $ret;
}

#############################
sub
SANYO_Z2000_Undefine($$)
{
	my($hash, $name) = @_;

	Log 5, "SANYO_Z2000_Undefine: Called for $name";

	RemoveInternalTimer($hash);
	DevIo_CloseDev($hash);

	return undef;
}

#############################
sub
SANYO_Z2000_Get($@)
{
	my ($hash, @a) = @_;
	my $what;

	return "argument is missing" if (int(@a) != 2);
	$what = $a[1];

	if ($what =~ /^(power|input|status|lamp)$/)
	{
		if(defined($hash->{READINGS}{$what}))
		{

			return $hash->{READINGS}{$what}{VAL};
		}
		else
		{
			return "no such reading: $what";
		}
	}
	else
	{
		return "Unknown argument $what, choose one of power videomute input status lamp";
	}
}

###################################
sub
SANYO_Z2000_Set($@)
{
	my ($hash, @a) = @_;

	my $what = $a[1];

	my $usage = "Unknown argument $what, choose one of on off toggle reconnect disconnect " .
		    "input:" . join(",", sort keys %inputs) . " " .
			"zoom:" . join(",", sort keys %zooms) . " " .
			"image:" . join(",", sort keys %images) . " " .
		    "rawCommand statusRequest";

	if ($what =~ /^(on|off)$/)
	{
		return SANYO_Z2000_Command_SetPower($hash, $what);
	}
	elsif ($what eq "toggle")
	{
		my $newPowerState = $powerStateTransition{$hash->{STATE}};
		return $newPowerState if (!defined($newPowerState));

		return SANYO_Z2000_Command_SetPower($hash, $newPowerState);
	}
	elsif ($what eq "input")
	{
		my $input = $a[2];
		return SANYO_Z2000_Command_SetInput($hash, $input);
	}
	elsif ($what eq "image")
	{
		my $image = $a[2];
		return SANYO_Z2000_Command_SetImage($hash, $image);
	}
	elsif ($what eq "zoom")
	{
		my $zoom = $a[2];
		return SANYO_Z2000_Command_SetZoom($hash, $zoom);
	}
	elsif ($what eq "rawCommand")
	{
		my $cmd = $a[2];
		$cmd = $a[2]." ".$a[3] if defined $a[3];
		SANYO_Z2000_SimpleWrite($hash, $cmd);
	}
	elsif ($what eq "statusRequest")
	{
	return SANYO_Z2000_Command_StatusRequest($hash);
	}
	elsif ($what eq "reconnect")
	{
		my $status = $hash->{READINGS}{"state"}{VAL};
		if($status ne "opened")
		{
			DevIo_OpenDev($hash, 0, "SANYO_Z2000_DoInit");
		}
		else
		{
			return "Device must be connected to disconnect!";
		}
	}
	elsif ($what eq "disconnect")
	{
			my $name = $hash->{NAME};
			DevIo_CloseDev($hash);
			$hash->{STATE} = "disconnected";
			setReadingsVal($hash, "state", "disconnected", TimeNow());
			Log 1, "$name: closed!";
	}
	else
	{
	return $usage;
	}
    return undef;
}

###################################
sub
SANYO_Z2000_Attr($@)
{
	my @a = @_;

	my $what = $a[2];
	if ($what eq "keepalive")
	{
		my $name = $a[1];
	    	my $hash = $defs{$name};

		my $keepalive = $a[3];

		my $ll5 = GetLogLevel($name, 5);
		Log $ll5, "SANYO_Z2000_Attr: Changing keepalive to <$keepalive> seconds";

		RemoveInternalTimer($hash);
		InternalTimer(gettimeofday() + $keepalive, "SANYO_Z2000_KeepAlive", $hash, 0);
	}

	return undef;
}

#####################################
sub
SANYO_Z2000_Shutdown($)
{
	my ($hash) = @_;

	Log 5, "SANYO_Z2000_Shutdown: Called";
}

#####################################
sub
SANYO_Z2000_UpdateConfig($)
{
	# this routine is called 5 sec after the last define of a restart
	# this gives FHEM sufficient time to fill in attributes
	# it will also be called after each manual definition
	# Purpose is to parse attributes and read config
	my ($hash) = @_;
	my $name = $hash->{NAME};

	my $webCmd	= AttrVal($name, "webCmd", "");
	if (!$webCmd)
	{
		$attr{$name}{webCmd} = "input";
	}

	my $keepalive = AttrVal($name, "keepalive", 5 * 60);

	RemoveInternalTimer($hash);
	InternalTimer(gettimeofday() + $keepalive, "SANYO_Z2000_KeepAlive", $hash, 0);
}

#####################################
sub
SANYO_Z2000_KeepAlive($)
{
	my ($hash) = @_;
	my $name = $hash->{NAME};

	my $ll5 = GetLogLevel($name, 5);
	Log $ll5, "SANYO_Z2000_KeepAlive: Called for $name";

	SANYO_Z2000_SimpleWrite($hash, "CR0");

	my $keepalive = AttrVal($name, "keepalive", 5 * 60);

	RemoveInternalTimer($hash);
	InternalTimer(gettimeofday() + $keepalive, "SANYO_Z2000_KeepAlive", $hash, 0);
}

#####################################
sub
SANYO_Z2000_Command_SetPower($$)
{
	my ($hash, $power) = @_;
	my $name = $hash->{NAME};

	my $ll5 = GetLogLevel($name, 5);
	Log $ll5, "SANYO_Z2000_Command_SetPower: Called for $name";

	my $command = $commands{"power:".lc($power)};
	SANYO_Z2000_SimpleWrite($hash, $command);

	readingsBeginUpdate($hash);
	readingsBulkUpdate($hash, "power", $power);
	readingsEndUpdate($hash, 1);

	return undef;
}

#####################################
sub
SANYO_Z2000_Command_SetInput($$)
{
	my ($hash, $input) = @_;
	my $name = $hash->{NAME};

	my $ll5 = GetLogLevel($name, 5);
	Log $ll5, "SANYO_Z2000_Command_SetInput: Called for $name";

	my $command = $inputs{$input};
	SANYO_Z2000_SimpleWrite($hash, $command);
	readingsBeginUpdate($hash);
	readingsBulkUpdate($hash, "input", $input);
	readingsEndUpdate($hash, 1);

	return undef;
}

#####################################
sub
SANYO_Z2000_Command_SetImage($$)
{
	my ($hash, $image) = @_;
	my $name = $hash->{NAME};

	my $ll5 = GetLogLevel($name, 5);
	Log $ll5, "SANYO_Z2000_Command_SetImage: Called for $name";

	my $command = $images{$image};
	SANYO_Z2000_SimpleWrite($hash, $command);
	readingsBeginUpdate($hash);
	readingsBulkUpdate($hash, "input", $image);
	readingsEndUpdate($hash, 1);

	return undef;
}

#####################################
sub
SANYO_Z2000_Command_SetZoom($$)
{
	my ($hash, $zoom) = @_;
	my $name = $hash->{NAME};

	my $ll5 = GetLogLevel($name, 5);
	Log $ll5, "SANYO_Z2000_Command_SetImage: Called for $name";

	my $command = $zooms{$zoom};
	SANYO_Z2000_SimpleWrite($hash, $command);
	readingsBeginUpdate($hash);
	readingsBulkUpdate($hash, "input", $zoom);
	readingsEndUpdate($hash, 1);

	return undef;
}

#####################################
sub
SANYO_Z2000_Command_StatusRequest($)
{
 	my ($hash) = @_;

	my $name = $hash->{NAME};

	my $ll5 = GetLogLevel($name, 5);
	Log $ll5, "SANYO_Z2000_Command_StatusRequest: Called for $name";

	SANYO_Z2000_SimpleWrite($hash, "CR0");
	SANYO_Z2000_SimpleWrite($hash, "CR1");
	SANYO_Z2000_SimpleWrite($hash, "CR3");

	return undef;
}

1;
